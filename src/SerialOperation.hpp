#ifndef SERIAL_OPERATION_HPP
#define SERIAL_OPERATION_HPP

#include <stdint.h>
#include "SerialPort.hpp"
#include "ErrorChecker.hpp"
#include "RecordBook.hpp"
#include "AccessResult.hpp"

class SerialOperation
{
public:
	SerialOperation(uint8_t* input);
	~SerialOperation();
	
	void updateWithRequest(uint8_t* input);
	int dumpResponse(uint8_t* output);
	AccessResult serialTxRx(SerialPort* sp, ErrorChecker ec = ErrorChecker(NO_CHECK));
	void updateRecords(RecordBook& rb);
	
private:
	SerialPort *sp;
	ErrorChecker ec;
	AccessResult lastResult;
	int version;
	int pid;
	int timeout, responseDelay;
	int responseSize, payloadSize; // Checksum is not inclueded.
	int targetAddress;
	std::vector<uint8_t> *txPayload, *rxPayload;
	int64_t startTime, finishTime;
	
	void transmit();
	AccessResult receive();
	int64_t now64();
};

#endif // SERIAL_OPERATION_HPP
