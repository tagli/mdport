#include <cerrno>
#include <cstring>
#include "SerialPort.hpp"

using namespace std;

SerialPort::SerialPort(string dn, int br, parity_t p)
: deviceName(dn), baudRate(br), parity(p), fd(0), deviceOpen(false),
lastTxSize(0)
{
	updateBytePeriod();	
	gettimeofday(&lastTxTime, 0);
	timeout.tv_sec = 0;
	timeout.tv_usec = 0;
}

int SerialPort::open()
{
	if (deviceOpen) {
		std::cerr << "Port is already open!\n";
		return -2; //Already open
	}
	
	//Opening the serial port file
	int tempFd = ::open(deviceName.c_str(), O_RDWR | O_NOCTTY | O_NDELAY);
	if (tempFd < 0){
		cerr << strerror(errno) << "Unable to open port!" << endl;
		return -1;
	}
	fd = tempFd;
	
	fcntl(tempFd, F_SETFL, 0); // Blocking read()
	
	// Applying initial settings
	setBaudRate(baudRate);
	setParity(parity);
	
	// Other settings whose functions are not implemented yet
	struct termios options;
	
	// Getting the termios struct
	if (tcgetattr(tempFd, &options)){
		cerr << strerror(errno) << "Unable to get attributes!" << endl;
		return -1;
	}

	//c_cflag Settings (excluding the parity settings)
	options.c_cflag &= ~CSTOPB; //1 stop bit
	options.c_cflag &= ~CSIZE; //Bitmask for size setting
	options.c_cflag |= CS8; //8 bit data
	
	//c_lflag Settings
	options.c_lflag &= ~(ECHO | ECHOE | ISIG | ICANON);

	//c_iflag Settings
	options.c_iflag &= ~(IXON | IXOFF | IXANY | ICRNL); //Software flow control disabled

	//c_oflag Settings
	options.c_oflag &= ~(OPOST | OCRNL); //Raw output
	
	//c_cc Settings
	options.c_cc[VTIME] = 1;

	if (tcsetattr(tempFd, TCSADRAIN, &options)){
		cerr << strerror(errno) << "Unable to set attributes!" << endl;
		return -1;
	}

	deviceOpen = true;
	return 0;
}

SerialPort::~SerialPort()
{
	cout << "Killing SerialPort...\n";
	close();
}

int SerialPort::close()
{
	if (!deviceOpen) return -1; // TODO: Implement using exceptions.
	::close(fd);
	deviceOpen = false;
	return 0;
}

int SerialPort::write(const uint8_t* source, int size)
{
	if (!deviceOpen){ // TODO: Implement using exceptions.
		return -1;
		cout << "Device not open!\n";
	}
	registerTx(size);
	return ::write(fd, source, size);
}

int SerialPort::write(std::vector<uint8_t>& source) {
	int ret, size;
	size = source.size();
	// TODO: Optimize this shit by avoiding copying!
	uint8_t *buffer = new uint8_t[size];
	for (int i = 0; i < size; ++i) buffer[i] = source[i];
	ret = this->write(buffer, size);
	delete[] buffer;
	return ret;
}

int SerialPort::read(uint8_t* target, int size)
{
	if (!deviceOpen) { // TODO: Implement using exceptions.
		return -1;
		cout << "Device not open!\n";
	}
	
	int ret;
	timeval tl = timeout;
	fd_set readSet;
	FD_SET(fd, &readSet);
	ret = ::select(fd + 1, &readSet, NULL, NULL, &tl);
	if (ret == 0) return 0; // Indicates a timeout
	if (FD_ISSET(fd, &readSet)) {
		ret = ::read(fd, target, size);
		return ret;
	}
	return -1; // If code reaches here, we have a problem.
}

int SerialPort::sendCmd(const uint8_t* source, int size)
{
	if (parity != SPACE_PARITY) {
		// TODO: Implement it with exceptions.
		cerr << "sendCmd is not applicible!\n";
		return -1;
	}
	
	setParity(MARK_PARITY);
	int retVal = write(source, size);
	setParity(SPACE_PARITY);
	return retVal;
}

int SerialPort::flush()
{
	return tcflush(fd, TCIOFLUSH);
}

// Functions for termios options

void SerialPort::setBaudRate(int br)
{
	struct termios options;
	
	waitForTx();
	
	// Getting the port attributes
	if (tcgetattr(fd, &options)){
		cerr << strerror(errno) << "Unable to get attributes!" << endl;
		return;
	}
	
	if (cfsetispeed(&options, baudRate) || cfsetospeed(&options, baudRate)){
		cerr << strerror(errno) << "Unable to change speed settings!" << endl;
		return;
	}
	
	// Setting the port attributes
	if (tcsetattr(fd, TCSADRAIN, &options)){
		cerr << strerror(errno) << "Unable to set attributes!" << endl;
		return;
	}
}

void SerialPort::setParity(parity_t p)
{
	struct termios options;
	
	waitForTx();
	
	// Getting the port attributes
	if (tcgetattr(fd, &options)){
		cerr << strerror(errno) << "Unable to get attributes!" << endl;
		return;
	}
	
	switch (p) {
	case NO_PARITY:
		options.c_cflag &= ~PARENB; // Parity is disabled
		options.c_cflag &= ~CMSPAR; // Sticky parity is disabled
		break;
	case ODD_PARITY:
		options.c_cflag |= PARENB; // Parity is enabled
		options.c_cflag |= PARODD; // Odd parity
		options.c_cflag &= ~CMSPAR; // Sticky parity is disabled
		break;
	case EVEN_PARITY:
		options.c_cflag |= PARENB; // Parity is enabled
		options.c_cflag &= ~PARODD; // Even parity
		options.c_cflag &= ~CMSPAR; // Sticky parity is disabled
		break;
	case MARK_PARITY:
		options.c_cflag |= PARENB; // Parity is enabled
		options.c_cflag |= CMSPAR; // Sticky parity is enabled
		options.c_cflag |= PARODD; // Mark parity
		break;
	case SPACE_PARITY:
		options.c_cflag |= PARENB; // Parity is enabled
		options.c_cflag |= CMSPAR; // Sticky parity is enabled
		options.c_cflag &= ~PARODD; // Space parity
		break;
	}
	
	// Setting the port attributes
	if (tcsetattr(fd, TCSADRAIN, &options)){
		cerr << strerror(errno) << "Unable to set attributes!" << endl;
		return;
	}
	
	parity = p;
}

void SerialPort::setVmin(uint8_t vmin)
{
	struct termios options;
	
	waitForTx();
	
	// Getting the port attributes
	if (tcgetattr(fd, &options)){
		cerr << strerror(errno) << "Unable to get attributes!" << endl;
		return;
	}
	
	options.c_cc[VMIN] = vmin;
	
	// Setting the port attributes
	if (tcsetattr(fd, TCSADRAIN, &options)){
		cerr << strerror(errno) << "Unable to set attributes!" << endl;
		return;
	}
}

void SerialPort::setTimeout(long ms)
{
	timeout.tv_sec = ms / 1000;
	timeout.tv_usec = (ms % 1000) * 1000;
}

// Related with FTDI driver tcdrain() bug workaround

void SerialPort::registerTx(int nBytes)
{
	if (isTransmiting()) {
		lastTxSize += nBytes;
	}
	else {
		gettimeofday(&lastTxTime, 0);
		lastTxSize = nBytes;
	}
}

void SerialPort::waitForTx()
{
	while (isTransmiting()) {
		usleep(bytePeriod);
	}
}

bool SerialPort::isTransmiting()
{
	long txTime = lastTxSize * bytePeriod; // An estimated value
	timeval now;
	gettimeofday(&now, 0);
	long timePassed = timevalDiff(&lastTxTime, &now);
	
	if (timePassed > txTime) return false;
	else return true;
}

long SerialPort::timevalDiff(timeval* start, timeval* finish)
{
	long usec;
	usec = (finish->tv_sec - start->tv_sec) * 1e6;
	usec += (finish->tv_usec - start->tv_usec);
	return usec;
}

void SerialPort::updateBytePeriod() {
	int bitsPerSecond = 0;
	
	switch (baudRate) {
	case B115200:
		bitsPerSecond = 115200;
		break;
	case B19200:
		bitsPerSecond = 19200;
		break;
	case B9600:
		bitsPerSecond = 9600;
		break;
	//Maybe more options can be added here.
	}
	
	bytePeriod = (int)((11 + 0) * (1.0 / bitsPerSecond) * 1e6);
}
