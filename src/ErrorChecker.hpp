#ifndef ERRORCHECKER_HPP
#define ERRORCHECKER_HPP

#include <vector>
#include <stdint.h>

enum CheckerType {NO_CHECK, SUM8};

class ErrorChecker
{
public:
	ErrorChecker(CheckerType type);
	std::vector<uint8_t> generate(std::vector<uint8_t>& input);
	std::vector<uint8_t> generate(uint8_t* input, int size);
	int size();
	
private:
	CheckerType type;
};

#endif
