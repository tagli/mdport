#include "ErrorChecker.hpp"

ErrorChecker::ErrorChecker(CheckerType type)
: type(type)
{
}

std::vector<uint8_t> ErrorChecker::generate(std::vector<uint8_t>& input)
{
	std::vector<uint8_t> checkBytes;
	uint8_t sum8 = 0;
	
	switch (type) {
	case NO_CHECK:
		return checkBytes;
	case SUM8:
		for (auto it = input.begin(); it != input.end(); ++it) {
			sum8 += *it;
		}
		checkBytes.push_back(sum8);
		return checkBytes;
	}
}

std::vector<uint8_t> ErrorChecker::generate(uint8_t* input, int size)
{
	std::vector<uint8_t> checkBytes;
	uint8_t sum8 = 0;
	
	switch (type) {
	case NO_CHECK:
		return checkBytes;
	case SUM8:
		for (int i = 0; i < size; ++i) sum8 += input[i];
		checkBytes.push_back(sum8);
		return checkBytes;
	}
}

int ErrorChecker::size()
{
	switch (type) {
	case NO_CHECK:
		return 0;
	case SUM8:
		return 1;
	}
}
