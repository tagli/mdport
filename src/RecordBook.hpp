#ifndef RECORD_BOOK_HPP
#define RECORD_BOOK_HPP

#include <sqlite3.h>
#include <unistd.h>
#include <iostream>
#include <sys/time.h>
#include <stdint.h>
#include <vector>
#include "AccessResult.hpp"

struct PairRow
{
	int master, slave, tx, rx, timeout, error;
};

class RecordBook
{
public:
	RecordBook();
	~RecordBook();
	
	void record(int pid, int adr, int tx, int rx, AccessResult ar, 
		int64_t startTime, int64_t finishTime);
	std::vector<PairRow> getPairList(int64_t noOlderThan);
	
private:
	sqlite3 *database;
	sqlite3_stmt *updatePair, *insertPair, *insertTransfer;
	sqlite3_stmt *selectPair;
	
	int64_t now64();
};

#endif
