#include "RecordBook.hpp"

RecordBook::RecordBook()
: database(nullptr)
{
	int ret;
	// Opening the database
	if (sqlite3_open(":memory:", &database) != SQLITE_OK)
		std::cout << "Unable to open database!\n";
	
	// Creating the table structure
	ret = sqlite3_exec(database,
		"CREATE TABLE pairs(\
			master INTEGER NOT NULL,\
			slave INTEGER NOT NULL,\
			tx INTEGER,\
			rx INTEGER,\
			timeout INTEGER,\
			error INTEGER,\
			lastSeen INTEGER,\
			PRIMARY KEY (master, slave)\
		);", NULL, NULL, NULL);
		
	ret = sqlite3_exec(database,
		"CREATE TABLE masters(\
			pid INTEGER PRIMARY KEY,\
			name TEXT\
		);", NULL, NULL, NULL);
		
	ret = sqlite3_exec(database,
		"CREATE TABLE transfers(\
			master INTEGER NOT NULL,\
			slave INTEGER NOT NULL,\
			tx INTEGER,\
			rx INTEGER,\
			result INTEGER,\
			startTime INTEGER,\
			finishTime INTEGER\
		);", NULL, NULL, NULL);
	
		
	// Creating the triggers
	ret = sqlite3_exec(database,
		"CREATE TRIGGER oldRemover AFTER INSERT \
		ON transfers \
		BEGIN \
			DELETE FROM transfers \
			WHERE startTime < ((strftime('%s','now') - 2) * 1000000); \
		END;",
		NULL, NULL, NULL);
		
	// Preparing the statements
	// http://stackoverflow.com/questions/15277373/sqlite-upsert-update-or-insert
	ret = sqlite3_prepare_v2(database,
		"UPDATE pairs \
		SET \
			tx = tx + ?3, \
			rx = rx + ?4, \
			timeout = timeout + ?5, \
			error = error + ?6, \
			lastSeen = ?7 \
		WHERE master = ?1 AND slave = ?2;",
		-1, &updatePair, 0);
	if (ret != SQLITE_OK)
		std::cout << "Prepare error: " << sqlite3_errmsg(database) << std::endl;
		
	ret = sqlite3_prepare_v2(database,
		"INSERT OR IGNORE INTO pairs \
			(master, slave, tx, rx, timeout, error, lastSeen) \
		VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7);",
		-1, &insertPair, 0);
	if (ret != SQLITE_OK)
		std::cout << "Prepare error: " << sqlite3_errmsg(database) << std::endl;
		
	ret = sqlite3_prepare_v2(database,
		"INSERT INTO transfers \
			(master, slave, tx, rx, result, startTime, finishTime) \
		VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7);",
		-1, &insertTransfer, 0);
	if (ret != SQLITE_OK)
		std::cout << "Prepare error: " << sqlite3_errmsg(database) << std::endl;
		
	ret = sqlite3_prepare_v2(database,
		"SELECT master, slave, tx, rx, timeout, error \
		FROM pairs \
		WHERE lastSeen > ?1;",
		-1, &selectPair, 0);
	if (ret != SQLITE_OK)
		std::cout << "Prepare error: " << sqlite3_errmsg(database) << std::endl;
	
}

RecordBook::~RecordBook()
{
	sqlite3_finalize(updatePair);
	sqlite3_finalize(insertPair);
	sqlite3_finalize(insertTransfer);
	sqlite3_close(database);
}

void RecordBook::record(int pid, int adr, int tx, int rx, AccessResult ar,
	int64_t startTime, int64_t finishTime)
{
	int ret;
	int timeout = 0, error = 0;
	switch (ar) {
	case TIMEOUT:
		timeout = 1;
		break;
	case CHK_ERROR:
		error = 1;
		break;
	}
	
	sqlite3_int64 lastSeen = now64();
	
	// Updating the pairs table
	sqlite3_reset(updatePair);
	sqlite3_reset(insertPair);
	
	sqlite3_bind_int(updatePair, 1, pid); // master
	sqlite3_bind_int(updatePair, 2, adr); // slave
	sqlite3_bind_int(updatePair, 3, tx);
	sqlite3_bind_int(updatePair, 4, rx);
	sqlite3_bind_int(updatePair, 5, timeout);
	sqlite3_bind_int(updatePair, 6, error);
	sqlite3_bind_int64(updatePair, 7, lastSeen);
	
	sqlite3_bind_int(insertPair, 1, pid); // master
	sqlite3_bind_int(insertPair, 2, adr); // slave
	sqlite3_bind_int(insertPair, 3, tx);
	sqlite3_bind_int(insertPair, 4, rx);
	sqlite3_bind_int(insertPair, 5, timeout);
	sqlite3_bind_int(insertPair, 6, error);
	sqlite3_bind_int64(insertPair, 7, lastSeen);
	
	ret = sqlite3_step(updatePair);
	ret = sqlite3_step(insertPair);
	
	// Updating the transfers table
	sqlite3_reset(insertTransfer);
	
	sqlite3_bind_int(insertTransfer, 1, pid); // master
	sqlite3_bind_int(insertTransfer, 2, adr); // slave
	sqlite3_bind_int(insertTransfer, 3, tx);
	sqlite3_bind_int(insertTransfer, 4, rx);
	sqlite3_bind_int(insertTransfer, 5, ar); // result
	sqlite3_bind_int64(insertTransfer, 6, startTime);
	sqlite3_bind_int64(insertTransfer, 7, finishTime);
	
	ret = sqlite3_step(insertTransfer);
}

std::vector<PairRow> RecordBook::getPairList(int64_t noOlderThan)
{
	int ret;
	std::vector<PairRow> pairList;
	sqlite3_int64 now = now64();
	sqlite3_reset(selectPair);
	sqlite3_bind_int64(selectPair, 1, now - noOlderThan);
	
	while (true) {
		PairRow tempRow;
		ret = sqlite3_step(selectPair);
		if (ret != SQLITE_ROW) break;
		tempRow.master = sqlite3_column_int(selectPair, 0);
		tempRow.slave = sqlite3_column_int(selectPair, 1);
		tempRow.tx = sqlite3_column_int(selectPair, 2);
		tempRow.rx = sqlite3_column_int(selectPair, 3);
		tempRow.timeout = sqlite3_column_int(selectPair, 4);
		tempRow.error = sqlite3_column_int(selectPair, 5);
		pairList.push_back(tempRow);
	}
	
	return pairList;
}

int64_t RecordBook::now64()
{
	timeval now;
	gettimeofday(&now, 0);
	return (now.tv_sec * 1000000) + now.tv_usec;
}
