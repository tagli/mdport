#ifndef SERIALPORT_HPP_INCLUDED
#define SERIALPORT_HPP_INCLUDED

#include <termios.h>
#include <unistd.h>
#include <fcntl.h>
#include <iostream>
#include <string>
#include <vector>
#include <stdint.h>
#include <sys/time.h>
#include <sys/select.h>

using namespace std;

class SerialPort
{
public:
	// Public Enumerations
	enum parity_t {
		NO_PARITY,
		EVEN_PARITY,
		ODD_PARITY,
		MARK_PARITY,
		SPACE_PARITY
	};
	
	// Interface
	SerialPort(string dn, int br, parity_t p = NO_PARITY);
	~SerialPort();
	int open();
	int close();
	bool isOpen() const { return deviceOpen; }
	
	int write(const uint8_t* source, int size);
	int write(std::vector<uint8_t>& source);
	int read(uint8_t* target, int size);
	int sendCmd(const uint8_t* source, int size);
	int flush();
	
	// Termios Options
	void setBaudRate(int br);
	void setParity(parity_t p);
	void setVmin(uint8_t vmin);
	void setTimeout(long ms);

private:
	int fd;
	timeval timeout;
	string deviceName;
	int baudRate;
	bool deviceOpen;
	parity_t parity;
	
	// Related with FTDI driver tcdrain() bug workaround
	int bytePeriod; //in microseconds
	timeval lastTxTime;
	int lastTxSize;
	
	void registerTx(int nBytes);
	void waitForTx();
	bool isTransmiting();
	long timevalDiff(timeval* start, timeval* finish);
	void updateBytePeriod();
};

#endif // SERIALPORT_HPP_INCLUDED
