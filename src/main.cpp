#include <iostream>
#include <zmq.hpp>
#include <string>
#include <thread>
#include <mutex>
#include <stdint.h>
#include <unistd.h>
#include <ncurses.h>

#include "SerialOperation.hpp"
#include "ErrorChecker.hpp"
#include "RecordBook.hpp"

// Constants and Definitions
const int BUFFER_SIZE = 300;

// Global Variables
std::mutex recordLock;
SerialPort *sp;
CheckerType chkType;
RecordBook rb;

// Local Function Prototypes
void serialService();
CheckerType strToChkType(std::string s);
int baudRateConverter(int baudRate);

void serialService()
{
	zmq::context_t context(1);
	zmq::socket_t socket(context, ZMQ_REP);
	socket.bind("ipc:///tmp/mdport");
	
	while(true) {
		
		// Message Reception and Unpacking
		zmq::message_t request;
		socket.recv(&request);
		//std::cout << "Message Received!\n";
		SerialOperation op = SerialOperation((uint8_t*)request.data());
		AccessResult ar;
		
		// Transmitting & Receiving
		ar = op.serialTxRx(sp, chkType);
		
		// Packing and sending the response message
		uint8_t responseBuffer[BUFFER_SIZE];
		int respMsgSize = op.dumpResponse(responseBuffer);
		zmq::message_t response(respMsgSize);
		memcpy(response.data(), responseBuffer, respMsgSize);
		socket.send(response);
		
		// Updating the history
		recordLock.lock();
		op.updateRecords(rb);
		recordLock.unlock();
	}
}

int main(int argc, char *argv[])
{
	chkType = NO_CHECK;
	std::string portName;
	int baudRate = 115200;
	
	// Option parsing
	int c;
	while ((c = getopt(argc, argv, "p:b:e:")) != -1) {
		switch (c) {
		case 'p':
			portName = std::string(optarg);
			break;
		case 'b':
			baudRate = atoi(optarg);
			break;
		case 'e':
			chkType = strToChkType(std::string(optarg));
			break;
		case '?':
			std::cout << "Unknown option!\n";
			break;
		}
	}
	
	sp = new SerialPort("/dev/" + portName, baudRateConverter(baudRate),
		SerialPort::SPACE_PARITY);
	sp->open(); // TODO: Shit happens! Be ready for it!
	
	std::thread serverThread(serialService);
	
	// ncurses Initialization
	initscr();
	cbreak();
	keypad(stdscr, TRUE);
	noecho();
	WINDOW *tableWin = newwin(20, 75, 4, 0);
	
	// Printing the header
	mvprintw(0, 0, "/dev/%s", portName.c_str());
	mvprintw(1, 0, "%d", baudRate);
	
	// Printing the labels
	mvprintw(3, 0, "Master\tSlave\tTX\tRX\tT/O\tErr.");
	refresh();
	
	while (true) {
		// Printing the pairs table
		std::vector<PairRow> pairTable;
		recordLock.lock();
		pairTable = rb.getPairList(5e6);
		recordLock.unlock();
		werase(tableWin);
		wmove(tableWin, 0, 0);
		for (int i = 0; i < pairTable.size(); ++i) {
			wprintw(tableWin, "%d\t%d\t%d\t%d\t%d\t%d\n", pairTable[i].master,
				pairTable[i].slave, pairTable[i].tx, pairTable[i].rx,
				pairTable[i].timeout, pairTable[i].error);
		}
		wrefresh(tableWin);
		usleep(250e3);
	}

	endwin();
	
	serverThread.join();
}

CheckerType strToChkType(std::string s)
{
	if (s == "sum8") return SUM8;
	else return NO_CHECK;
}

int baudRateConverter(int baudRate)
{
	switch (baudRate) {
	case 115200:
		return B115200;
	case 19200:
		return B19200;
	default:
		std::cout << "Unknown baud date! Using default value 9600\n";
	case 9600:
		return B9600;
	}
}
