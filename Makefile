VPATH = src

iFlags = -Isrc
cFlags = -std=c++11
libFlags = -lzmq -lpthread -lsqlite3 -lncurses
srcFiles = main.cpp SerialPort.cpp SerialOperation.cpp ErrorChecker.cpp
srcFiles += RecordBook.cpp
objFiles = $(srcFiles:.cpp=.o)

mdport: $(objFiles)
	g++ $^ $(cFlags) $(iFlags) $(libFlags) -o $@
	
%.o: %.cpp
	g++ -c -MMD -MP $(cFlags) $(iFlags) $< -o $@

-include $(srcFiles:%.cpp=%.d)

.PHONY : clean

clean:
	-rm -f mdport *.o *.d
