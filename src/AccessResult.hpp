#ifndef ACCESS_RESULT_HPP
#define ACCESS_RESULT_HPP

#include <stdint.h>

enum AccessResult : int8_t {SUCCESS = 0, TIMEOUT = 1, CHK_ERROR = 2};

#endif
