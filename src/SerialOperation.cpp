#include "SerialOperation.hpp"

SerialOperation::SerialOperation(uint8_t* input)
: ec(ErrorChecker(NO_CHECK)), txPayload(nullptr), rxPayload(nullptr)
{
	updateWithRequest(input);
}

SerialOperation::~SerialOperation()
{
	delete txPayload;
	delete rxPayload;
}

void SerialOperation::updateWithRequest(uint8_t* input)
{
	delete[] txPayload;
	version = *((int32_t*)(input));
	switch (version) {
	case 1:
	// ver(4) pid(4) timeout(4) respSize(1) adr(1) plSize(1) pl(n)
		pid = *((int32_t*)(input + 4));
		timeout = *((int32_t*)(input + 8));
		responseSize = *((uint8_t*)(input + 12));
		targetAddress = *((uint8_t*)(input + 13));
		payloadSize = *((uint8_t*)(input + 14));
		txPayload = new std::vector<uint8_t>(payloadSize);
		for (int i = 0; i < payloadSize; ++i) {
			txPayload->at(i) = input[i + 15];
		}
		break;
	}
}

int SerialOperation::dumpResponse(uint8_t* output)
{
	switch (version) {
	case 1:
	//ver(4) res(1) delay(4) plSize(1) pl(n)
		*((int*)(output)) = version;
		*((AccessResult*)(output + 4)) = lastResult;
		*((int*)(output + 5)) = responseDelay;
		*(output + 9) = (uint8_t)responseSize;
		for (int i = 0; i < responseSize; ++i) {
			*(output + 10 + i) = rxPayload->at(i);
		}
		return responseSize + 10;
	}
}

AccessResult SerialOperation::serialTxRx(SerialPort* sp, ErrorChecker ec)
{
	this->ec = ec;
	this->sp = sp;
	sp->flush();
	transmit();
	if (responseSize > 0) {
		lastResult = receive();
		return lastResult;
	}
	else return (lastResult = SUCCESS);
}

void SerialOperation::transmit()
{
	uint8_t adr = (uint8_t)targetAddress;
	std::vector<uint8_t> bytesToCheck(payloadSize + 1);
	bytesToCheck.push_back(adr);
	for (int i = 0; i < payloadSize; ++i) {
		bytesToCheck.push_back(txPayload->at(i));
	}
	std::vector<uint8_t> checksum = ec.generate(bytesToCheck);
	startTime = now64();
	finishTime = 0; // TODO: This must be a calculated value.
	sp->sendCmd(&adr, sizeof(adr)); // Addressing the device
	sp->write(*txPayload); // Sending the payload (request)
	sp->write(checksum); // Sending the checksum
}

AccessResult SerialOperation::receive()
{
	delete rxPayload;
	int readResult;
	bool checksumMatch = false;
	uint8_t *rxBuffer = new uint8_t[responseSize + ec.size()];
	rxPayload = new std::vector<uint8_t>(responseSize);
	
	// Receiving from serial port
	sp->setVmin(responseSize + ec.size());
	sp->setTimeout(timeout);
	readResult = sp->read(rxBuffer, responseSize + ec.size()); // Receiving payload + checksum
	finishTime = now64();
	
	if (readResult > 0) {
	
		// Separatig checksum from data
		std::vector<uint8_t> receivedChecksum(ec.size());
		for (int i = 0; i < responseSize; ++i) {
			rxPayload->at(i) = rxBuffer[i];
		}
		for (int i = 0; i < ec.size(); ++i) {
			receivedChecksum.at(i) = rxBuffer[i + responseSize];
		}
		
		// Error Checking
		checksumMatch = (receivedChecksum == ec.generate(rxBuffer, responseSize));
		
		// TODO
		responseDelay = 0; // FIX IT!
	}
	
	delete[] rxBuffer;
	if (readResult == 0) {
		responseSize = 0;
		return TIMEOUT;
	}
	else if (checksumMatch) return SUCCESS;
	else return CHK_ERROR;
}

void SerialOperation::updateRecords(RecordBook& rb)
{
	rb.record(pid, targetAddress, payloadSize, responseSize, lastResult,
		startTime, finishTime);
}

int64_t SerialOperation::now64()
{
	timeval now;
	gettimeofday(&now, 0);
	return (now.tv_sec * 1000000) + now.tv_usec;
}
